#%%
import matplotlib.pyplot as plt
import numpy as np
import json
import pandas as pd

#%%
experiments = [
    {'filename': 'first_catapult\\non-magenta_backup.txt', 'is_distance_sensor': True, 'is_servo': False, 'is_servo_trigger': False, 'is_white': False},
    {'filename': 'first_catapult\magenta_backup.txt', 'is_distance_sensor': True, 'is_servo': False, 'is_servo_trigger': False, 'is_white': False},
    {'filename': 'second_catapult\\black.txt', 'is_distance_sensor': False, 'is_servo': True, 'is_servo_trigger': False, 'is_white': False},
    {'filename': 'second_catapult\white.txt', 'is_distance_sensor': False, 'is_servo': True, 'is_servo_trigger': False, 'is_white': True},
    {'filename': 'third_catapult\green.txt', 'is_distance_sensor': True, 'is_servo': False, 'is_servo_trigger': False, 'is_white': False},
    {'filename': 'third_catapult\magenta.txt', 'is_distance_sensor': True, 'is_servo': False, 'is_servo_trigger': False, 'is_white': False},
    {'filename': 'fourth_catapult\\non-magenta.txt', 'is_distance_sensor': True, 'is_servo': False, 'is_servo_trigger': False, 'is_white': False},
    {'filename': 'fifth_catapult\magneta.txt', 'is_distance_sensor': True, 'is_servo': False, 'is_servo_trigger': True, 'is_white': False},
    {'filename': 'fifth_catapult\\non-magenta.txt', 'is_distance_sensor': True, 'is_servo': False, 'is_servo_trigger': True, 'is_white': False}
]
save_to_file = True

for experiment in experiments:
    experiment_filename = experiment['filename']
    is_servo = experiment['is_servo']
    is_distance_sensor = experiment['is_distance_sensor']
    is_servo_trigger = experiment['is_servo_trigger']
    is_white = experiment['is_white']

    with open(experiment_filename, 'r') as data:
        mes_data = data.read()    

    # %%
    without_header=''.join(mes_data.split('Start data aquisition\nNew test'))
    tests = without_header.split('New test')
    # %%
    experiment_code = 'first_catapult'
    columns_names = ['Time[ms]', 'acc_x', 'acc_y', 'acc_z', 'gyro_x', 'gyro_y', 'gyro_z']
    test_id = -1

    if is_servo == True:
        imu_data = [x for x in tests[test_id].split('\n') if x[:2]!="MS"]
        servo_data = [x for x in tests[test_id].split('\n') if x[:2]=="MS"]
        servo_data_frame = pd.DataFrame([x.split('\t') for x in servo_data], columns=['Time[ms]', 'req_traj'])
        servo_data_frame['Time[ms]'] = servo_data_frame['Time[ms]'].map(lambda x: x.lstrip('MS:')).astype(np.long)
        if is_white == True:
            servo_data_frame['req_traj'] = servo_data_frame['req_traj'].map(lambda x: x.lstrip('Req_traj')).astype(int)
        else:
            servo_data_frame['req_traj'] = servo_data_frame['req_traj'].map(lambda x: x.lstrip('Req_traj: ')).astype(int)
        servo_data_frame = servo_data_frame.set_index('Time[ms]')
    if is_distance_sensor == True:
        if is_servo_trigger == True:
            imu_data = [x for x in tests[test_id].split('\n') if x[:2] != "MD" and x[:4] != "Star"]
            servo_data = [x for x in tests[test_id].split('\n') if x[:4] == "Star"]
            servo_data_frame = pd.DataFrame([x.split('\t')[-1] for x in servo_data], columns=['Time[ms]'])
        else:
            imu_data = [x for x in tests[test_id].split('\n') if x[:2]!="MD"]
        distance_sensor_data = [x for x in tests[test_id].split('\n') if x[:2]=="MD"]
        distance_sensor_data_frame = pd.DataFrame([x.split('\t') for x in distance_sensor_data], columns=['Time[ms]', 'displacement'])
        distance_sensor_data_frame['Time[ms]'] = distance_sensor_data_frame['Time[ms]'].map(lambda x: x.lstrip('MD:')).astype(np.long)
        distance_sensor_data_frame['displacement'] = distance_sensor_data_frame['displacement'].astype(int)
        distance_sensor_data_frame = distance_sensor_data_frame.set_index('Time[ms]')

    data_frames = pd.DataFrame([x.split('\t') for x in imu_data], columns=columns_names)
    data_frames_without_empty = data_frames[1:-1]
    # df = df[df.line_race != 0]
    data_frame = data_frames_without_empty
    #%%

    data_frame['Time[ms]'] = data_frame['Time[ms]'].map(lambda x: x.lstrip('M:')).astype(np.long)
    data_frame['acc_x'] = data_frame['acc_x'].map(lambda x: x.lstrip('acc:')).astype(float)
    data_frame['acc_y'] = data_frame['acc_y'].astype(float)
    data_frame['acc_z'] = data_frame['acc_z'].astype(float)
    data_frame['gyro_x'] = data_frame['gyro_x'].map(lambda x: x.lstrip('gyr:')).astype(float)
    data_frame['gyro_y'] = data_frame['gyro_y'].astype(float)
    data_frame['gyro_z'] = data_frame['gyro_z'].astype(float)
    # data_frame['gyro_y'] = data_frame[data_frame['gyro_y'] <1000]['gyro_y']
    data_frame = data_frame.set_index('Time[ms]')
    if save_to_file is True:
        data_frame.to_csv(f'{experiment_filename[:-4]}_imu_data.csv')
        if is_servo == True:
            servo_data_frame.to_csv(f'{experiment_filename[:-4]}_servo_req_trajectory.csv')
        if is_distance_sensor == True:
            distance_sensor_data_frame.to_csv(f'{experiment_filename[:-4]}_distance_sensor.csv')
        if is_servo_trigger == True:
            servo_data_frame.to_csv(f'{experiment_filename[:-4]}_servo_trigger_time.csv')

    # %%
    # motor_data = pd.read_csv('second_catapult\motor2_03_03_2021_08_07_-2100_1700.csv')
    # %%
    # motor_data = motor_data[['actual_velocity', 'current_position', 'time diff[ms]']]
    # motor_data['time diff[ms]'] = motor_data['time diff[ms]'].expanding(1).sum()
    # %%
    # motor_data = motor_data.set_index('time diff[ms]')
    # %%

    # %%

    # data_frame.plot(figsize=(15,10))
    # data_frame.loc[:16000]['gyro_z'].plot(figsize=(15,10))
    # (-motor_data['actual_velocity'].loc[:16000]*300/16/60).plot()
    # %%
