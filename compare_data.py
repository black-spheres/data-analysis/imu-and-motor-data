#%%
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

motor_data_0 = pd.read_csv('first_catapult\motor1_02_03_2021_20_10_-2000_3300.csv')
motor_data_0 = motor_data_0[['actual_velocity', 'current_position', 'time diff[ms]']]
motor_data_0['time diff[ms]'] = motor_data_0['time diff[ms]'].expanding(1).sum()
motor_data_0 = motor_data_0.set_index('time diff[ms]')

motor_data_1 = pd.read_csv('first_catapult\motor2_02_03_2021_20_10_-2000_3300.csv')
motor_data_1 = motor_data_1[['actual_velocity', 'current_position', 'time diff[ms]']]
motor_data_1['time diff[ms]'] = motor_data_1['time diff[ms]'].expanding(1).sum()
motor_data_1 = motor_data_1.set_index('time diff[ms]')
# %%
plt.figure()

(motor_data_0.loc[:7500]['current_position']).plot(figsize=(10,5))
(motor_data_1.loc[:7500]['current_position']).plot(figsize=(10,5))
plt.xlabel("Time[ms]")
plt.ylabel("Motor position [Inc]")
# plt.ylim([-2500, -2100])
# %%
imu_data_non_magenta = pd.read_csv('fifth_catapult\\non-magenta_imu_data.csv', index_col=0)
imu_data_magenta = pd.read_csv('fifth_catapult\magneta_imu_data.csv', index_col=0)
# imu_data_non_magenta.loc[:14000]['gyro_z'].plot()
# imu_data_magenta.loc[:14000]['gyro_z'].plot()

# %%
from scipy import io

spheres_trajectories = io.loadmat('vision_system_output\spheres_trajectories_exp_4_5.mat')

sphere_0 = spheres_trajectories['sphere_xyz_center_s1_e5']
sphere_1 = spheres_trajectories['sphere_xyz_center_s2_e5']
# %%
sphere_0_x = sphere_0[:,0]
sphere_0_y = sphere_0[:,1]
sphere_0_z = sphere_0[:,2]

sphere_1_x = sphere_1[:,0]
sphere_1_y = sphere_1[:,1]
sphere_1_z = sphere_1[:,2]
# %%
import matplotlib.pyplot as plt
fig = plt.figure(figsize=(15,15))
ax = fig.add_subplot(projection='3d')
ax.plot(sphere_0_x, sphere_0_y, sphere_0_z)
ax.plot(sphere_1_x, sphere_1_y, sphere_1_z)
# %%
plt.plot(sphere_0_x, sphere_0_z)
plt.plot(sphere_1_x, sphere_1_z)
# %%
plt.plot(sphere_0_y, sphere_0_z)
plt.plot(sphere_1_y, sphere_1_z)
# %% distances 
sphere_0_dist = [np.linalg.norm(x - sphere_0[0]) for x in sphere_0]
sphere_1_dist = [np.linalg.norm(x - sphere_1[0]) for x in sphere_1]

# %%
plt.plot(sphere_0_dist)
plt.plot(sphere_1_dist)
plt.legend(['sphere_0', 'sphere_1'])
# %%
imu_data_non_magenta.loc[:14000]['gyro_z'].plot()
imu_data_magenta.loc[:14000]['gyro_z'].plot()
plt.legend(['non_magenta', 'magenta'])

# %%
motor_data_0.loc[:14000]['actual_velocity'].plot()
motor_data_1.loc[:14000]['actual_velocity'].plot()
plt.legend(['sphere_0', 'sphere_1'])

# %%
ug_time_sphere_0 = np.linspace(0, 9050, len(sphere_0)-608)
ug_time_sphere_1 = np.linspace(0, 9050, len(sphere_1)-608)

# %%
plt.figure(figsize=(15,10))
plt.plot(ug_time_sphere_1+7600, sphere_1_x[608:])
(imu_data_magenta/5).loc[7600:16500]['gyro_z'].plot()
plt.legend(['sphere_1_x (VS)', 'sphere_1_gyro_z/5 (IMU)'])
# %%
imu_data_magenta_copy = imu_data_magenta.copy()
imu_data_magenta_copy.index = imu_data_magenta_copy.index - 135
imu_data_magenta_copy = imu_data_magenta_copy.drop([7061])
imu_v1 = imu_data_magenta_copy.loc[4400:5000].mean()['gyro_z']
motor_v1 = -motor_data_1.loc[4400:5000].mean()['actual_velocity']/16
scale_coef = imu_v1/motor_v1
# %%
ccs_imu = pd.read_csv('CCS_data\CCS_acc.csv', index_col=0)
ccs_imu.index = ccs_imu.index*1000-135

#%%
plt.rcParams.update({'font.size': 16})
fig, ax1 = plt.subplots()
fig.set_figheight(8)
fig.set_figwidth(15)
ax2 = ax1.twinx()
ax1.plot((imu_data_magenta_copy/scale_coef/60)['gyro_z'],linewidth=2)
ax1.plot((-motor_data_1.loc[:8600]/(16)/60)['actual_velocity'],linewidth=2)
ax1.set_ylim([-2.5, 5])
ax2.plot((ccs_imu.loc[:13000]['Accel_Z']/9.81), 'g', linewidth=2)
ax2.set_ylim([-170, 40])
ax1.set_xlim([0,13000])
ax2.set_xlim([0,13000])

ax1.set_ylabel('Angular velocity [Hz]')
ax2.set_ylabel('Acceleration [g]', color='g')
# (ccs_imu.loc[:13000]['Accel_Z']/200).plot()
# (-motor_data_1.loc[:8500]/(16)/60)['actual_velocity'].plot(linewidth=1)
fig.legend(["Sphere's angular velocity about Z axis", "Release Mechanism's angular velocity about Z axis", "Capsule's accelerometer indication about Z axis"], loc='upper right', bbox_to_anchor=(0.75, 0.82))
ax1.set_xlabel('Time [ms]')
fig.savefig('pics\gyro_vs_motor_vs_ccs_acc_3.eps', format='eps')
# ax2.legend([ "Capsule about Z axis"])
# plt.xlabel('Time [ms]')
# plt.ylabel('Angular velocity [Hz]')

#%%
plt.savefig('pics\gyro_vs_motor_vs_ccs_acc.eps', format='eps')
plt.savefig('pics\gyro_vs_motor_vs_ccs_acc.png', format='png', dpi=600)


# %%
