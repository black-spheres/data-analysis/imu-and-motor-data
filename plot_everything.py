#%%
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import json

with open('experiments_info.json') as f:
    d = json.load(f)
    experiments = d["experiments"]

scale_coef = 6.129277662002041
# axs[0].bar(names, values)
# axs[1].scatter(names, values)
# axs[2].plot(names, values)
# %%
fig, axs = plt.subplots(1, 5, figsize=(25, 5), sharey=True)
axs[0].set_ylabel('Angular velocity [Hz] about Z axis')
for i, experiment in enumerate(experiments):
    for sphere in experiment:
        gyro_data = pd.read_csv(sphere["imu_filename"], index_col=0)
        data_to_plot = gyro_data.loc[:15000]['gyro_z']/scale_coef/60
        axs[i].plot(data_to_plot)
    axs[i].set_title(f'Experiment {i+1}')

    
    axs[i].set_xlabel('Time [ms]')
fig.tight_layout()
fig.legend(["Sphere 1", "Sphere 2"], loc='upper right', bbox_to_anchor=(0.75, 0.4))
fig.savefig('pics\gyro_exp.eps', format='eps')
# %%
fig, axs = plt.subplots(1, 5, figsize=(25, 5), sharey=False)
axs[0].set_ylabel('Angular velocity [Hz] about Z axis')
for i, experiment in enumerate(experiments):

    for sphere in experiment[:-1] if len(experiment) > 1 else experiment:
        gyro_data = pd.read_csv(sphere["imu_filename"], index_col=0)
        if i != 1:
            data_to_plot = gyro_data.loc[7900:15000]['gyro_z']/scale_coef/60
        else:
            data_to_plot = gyro_data.loc[11200:14000]['gyro_z']/scale_coef/60

        axs[i].plot(data_to_plot)
    

    axs[i].set_title(f'Experiment {i+1}')

    
    axs[i].set_xlabel('Time [ms]')
    if i == 2:
        axs[i].set_ylim([1.5, 1.6])
fig.tight_layout()
fig.savefig('pics\gyro_exp_zoom_sphere_1.eps', format='eps')
# %%
fig, axs = plt.subplots(1, 4, figsize=(25, 5), sharey=False)
axs[0].set_ylabel('Angular velocity [Hz] about Z axis')
for i, experiment in enumerate(experiments):
    if i>3:
        i = i-1
    for sphere in experiment[1:]:
        gyro_data = pd.read_csv(sphere["imu_filename"], index_col=0)
        if i != 1:
            data_to_plot = gyro_data.loc[7900:15000]['gyro_z']/scale_coef/60
        else:
            data_to_plot = gyro_data.loc[11200:14000]['gyro_z']/scale_coef/60

        axs[i].plot(data_to_plot)
    

    axs[i].set_title(f'Experiment {i+1}' if i < 3 else f'Experiment {i+2}')

    
    axs[i].set_xlabel('Time [ms]')
    if i == 2:
        axs[i].set_ylim([1.85, 1.95])
fig.tight_layout()
fig.savefig('pics\gyro_exp_zoom_sphere_2.eps', format='eps')
# %%
